﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Cellular_automaton
{
    class ApplicationViewModel : INotifyPropertyChanged
    {
        private static EventWaitHandle eventWaitHandle = new AutoResetEvent(false);
        private static EventWaitHandle eventWaitHandleScale = new AutoResetEvent(false);
        static Mutex mutex = new Mutex(false, "SyncTool");

        private BackgroundWorker worker;
        private BackgroundWorker Scale;

        public ObservableCollection<string> Saves { get; set; }

        private int curStep;
        public int CurStep
        {
            get { return curStep; }
            set { curStep = value; OnPropertyChanged("CurStep"); }
        }

        private int allSteps;
        public int AllSteps
        {
            get { return allSteps; }
            set { allSteps = value; OnPropertyChanged("AllSteps"); }
        }

        private Field field;
        public Field ThisField
        {
            get { return field; }
        }

        private int cellSize;
        public int CellSize
        {
            get { return cellSize; }
            set
            {
                cellSize = value;
                OnPropertyChanged("CellSize");
            }
        }

        private int xSize;
        public int XSize
        {
            get { return xSize; }
            set
            {
                xSize = value;
                OnPropertyChanged("XSize");
            }
        }

        private int ySize;
        public int YSize
        {
            get { return ySize; }
            set
            {
                ySize = value;
                OnPropertyChanged("YSize");
            }
        }

        private string fileToLoad;
        public string FileToLoad
        {
            get { return fileToLoad; }
            set
            {
                fileToLoad = value;
                OnPropertyChanged("FileToLoad");
            }
        }

        private string saveName;
        public string SaveName
        {
            get { return saveName; }
            set
            {
                saveName = value;
                OnPropertyChanged("SaveName");
            }
        }

        public ApplicationViewModel()
        {
            curStep = 0;
            allSteps = 0;

            field = new Field(0, 0);
            cellSize = 10;
            field.Speed = 10;

            FileToLoad = "";
            SaveName = "";
            Saves = new ObservableCollection<string>();

            string[] files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory);
            foreach (string s in files)
                if (Path.GetExtension(s) == ".txt" && Path.GetFileNameWithoutExtension(s) != "tempfile") Saves.Add(Path.GetFileNameWithoutExtension(s));

            worker = new BackgroundWorker();
            worker.RunWorkerCompleted += RunWorkerCompleted;
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;
    
            Scale = new BackgroundWorker();
            Scale.ProgressChanged += RunReportProgress1;
            Scale.WorkerSupportsCancellation = true;
            Scale.WorkerReportsProgress = true;
        }

        private RelayCommand changeSize;
        public RelayCommand ChangeSize
        {
            get
            {
                return changeSize ??
                  (changeSize = new RelayCommand(obj =>
                  {
                      if (CellSize >= 5 && CellSize <= 20)
                      {
                          int old = cellSize;
                          CellSize = old + Convert.ToInt32(obj.ToString());
                          if (CellSize > 20) CellSize--;
                          if (CellSize < 5) CellSize++;
                          else 
                          {
                              object[] pr = new object[] { cellSize, old, mutex, eventWaitHandleScale };

                              Scale.DoWork += field.PosTransform;
                              Scale.RunWorkerAsync(pr);
                          }

                      }
                     
                  }, (obj) => !Scale.IsBusy));
            }
        }

        private RelayCommand loadFile;
        public RelayCommand LoadFile
        {
            get
            {
                return loadFile ??
                  (loadFile = new RelayCommand(obj =>
                  {
                      if (File.Exists(FileToLoad + ".txt"))
                      {
                          using (FileStream fstream = new FileStream(FileToLoad + ".txt", FileMode.Open))
                          {
                              int flag = 0;
                              int tempCols = 0, tempRows = 0;
                              char ch;

                              fstream.Seek(0, SeekOrigin.Begin);

                              while (true)
                              {
                                  ch = (char)fstream.ReadByte();
                                  if (ch == -1 || ch == '.') break;
                                  else
                                  {
                                      if (ch == 'x')
                                      {
                                          flag++;
                                          continue;
                                      }
                                      if (flag == 0)
                                          tempCols = tempCols * 10 + (int)(ch - '0');
                                      if (flag == 1)
                                          tempRows = tempRows * 10 + (int)(ch - '0');
                                  }
                              }

                              AllSteps = 0;
                              int Size = tempCols * tempRows;
                              while (fstream.Seek(Size, SeekOrigin.Current) <= fstream.Length) AllSteps++;

                              CurStep = allSteps;
                              fstream.Seek(-Size, SeekOrigin.End);

                              // считываем четыре символов с текущей позиции
                              byte[] output = new byte[Size];
                              fstream.Read(output, 0, output.Length);
                              // декодируем байты в строку
                              string textFromFile = Encoding.Default.GetString(output);

                              if(ThisField.Columns == tempCols && ThisField.Rows == tempRows)
                              {
                                  for (int i = 0; i < ThisField.Rows; i++)
                                      for (int j = 0; j < ThisField.Columns; j++)
                                          ThisField.CellField[i * ThisField.Columns + j].CellState = (State)Enum.Parse(typeof(State), textFromFile[i * ThisField.Columns + j].ToString());
                              }
                              else
                              {
                                  ThisField.CellField.Clear();
                                  ThisField.Columns = tempCols;
                                  ThisField.Rows = tempRows;

                                  Cell temp;
                                  for (int i = 0; i < ThisField.Rows; i++)
                                  {
                                      for (int j = 0; j < ThisField.Columns; j++)
                                      {
                                          temp = new Cell();
                                          temp.PosX = i * cellSize;
                                          temp.PosY = j * cellSize;
                                          temp.CellState = (State)Enum.Parse(typeof(State), textFromFile[i * ThisField.Columns + j].ToString());
                                          ThisField.CellField.Add(temp);
                                      }
                                  } 
                              }

                              File.Delete("tempfile.txt");
                              File.Copy(FileToLoad + ".txt", "tempfile.txt");

                              XSize = ThisField.Rows * CellSize;
                              YSize = ThisField.Columns * CellSize;
                          }
                      }
                  }, (obj) => FileToLoad != ""));
            }
        }

        private RelayCommand save;
        public RelayCommand Save
        {
            get
            {
                return save ??
                  (save = new RelayCommand(obj =>
                  {
                      if (allSteps == 0) SaveField(false);
                      if (File.Exists($"{SaveName}.txt")) File.Delete($"{SaveName}.txt");
                      File.Copy("tempfile.txt", $"{SaveName}.txt");
                      Saves.Remove($"{SaveName}");
                      Saves.Add($"{SaveName}");
                  }, (obj) => SaveName != "" && !worker.IsBusy));
            }
        }

        public void SaveField(bool append)
        {
            StreamWriter sw = null;

            try
            {
                if (!File.Exists("tempfile.txt") || ThisField.CellField.Count == 0) append = false;

                sw = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tempfile.txt"), append, System.Text.Encoding.Default);

                if(!append)
                    sw.Write($"{ThisField.Rows}x{ThisField.Columns}.");

                foreach (Cell temp in ThisField.CellField)
                {
                    sw.Write((int)temp.CellState);
                    sw.Flush();
                }
                
            }
            catch(Exception ex) { }
            finally
            {
                if (sw != null)
                    sw.Close();
            }
        }

        public void Load(int nstep)
        {
            using (FileStream fstream = new FileStream("tempfile.txt", FileMode.Open))
            {
                int smove = allSteps - nstep + 1;

                int fmove = ThisField.Rows * ThisField.Columns * smove;
                fstream.Seek(-fmove, SeekOrigin.End);

                byte[] output = new byte[ThisField.Rows * ThisField.Columns];
                fstream.Read(output, 0, output.Length);
                string textFromFile = Encoding.Default.GetString(output);

                for (int i = 0; i < ThisField.Rows; i++)
                    for (int j = 0; j < ThisField.Columns; j++)
                        ThisField.CellField[i * ThisField.Columns + j].CellState = (State)Enum.Parse(typeof(State), textFromFile[i * ThisField.Columns + j].ToString());
            }
        }

        private RelayCommand settings;
        public RelayCommand Settings
        {
            get
            {
                return settings ??
                  (settings = new RelayCommand(obj =>
                  {
                      int x = 0;
                      SettingsViewModel VM = new SettingsViewModel(ThisField.Rows, ThisField.Columns);
                      foreach (Cell temp in ThisField.CellField)
                      { 
                          VM.ThisField.CellField[x].CellState = temp.CellState;
                          x++;
                      }
                      ViewShower.Show(0, VM, true, 
                          b => {
                                ThisField.CellField.Clear();
                                  ThisField.Rows = VM.ThisField.Rows;
                                  ThisField.Columns = VM.ThisField.Columns;
                                  CellSize = VM.CellSize;
                                  for (int i = 0; i < VM.ThisField.CellField.Count; i++)
                                      ThisField.CellField.Add(VM.ThisField.CellField.ElementAt(i));

                                  CurStep = 1; AllSteps = 1;
                                  XSize = ThisField.Rows * CellSize;
                                  YSize = ThisField.Columns * CellSize;
                                  File.Delete("tempfile.txt");
                                  SaveField(false);
                          });
                  }, (obj) => !worker.IsBusy));
            }
        }

        private RelayCommand step;
        public RelayCommand Step
        {
            get
            {
                return step ??
                  (step = new RelayCommand(obj =>
                  {
                      string pr = obj as string;

                      if(pr == "Prev")
                      {
                          if(curStep - 2 >= 0)                          
                              Load(--CurStep);
                      }
                      else 
                      {
                          if (curStep == allSteps)
                          {
                              worker.DoWork += field.UpdateField;
                              worker.RunWorkerAsync();
                          }
                          else
                              Load(++CurStep);
                      }
                  }, (obj) => ThisField.CellField.Count() > 0 && !worker.IsBusy && (CurStep >= 0 && CurStep <= AllSteps)));
            }
        }

        private RelayCommand getStep;
        public RelayCommand GetStep
        {
            get
            {
                return getStep ??
                  (getStep = new RelayCommand(obj =>
                  {
                      Load(CurStep);
                  }, (obj) => curStep >= 1 && curStep <= allSteps && !worker.IsBusy));
            }
        }

        private RelayCommand startSimulation;
        public RelayCommand StartSimulation
        {
            get
            {
                return startSimulation ??
                  (startSimulation = new RelayCommand(obj =>
                  {
                      Load(AllSteps);
                      CurStep = AllSteps;
                      object[] pr = new object[] { mutex, eventWaitHandle }; 
                      worker.DoWork += field.Simulation;
                      worker.ProgressChanged += RunReportProgress;
                      worker.RunWorkerAsync(pr);
                  }, (obj) => ThisField.CellField.Count() > 0 && !worker.IsBusy));
            }
        }

        private RelayCommand cancel;
        public RelayCommand Cancel
        {
            get
            {
                return cancel ??
                  (cancel = new RelayCommand(obj =>
                  {
                      worker.CancelAsync();
                  }));
            }
        }

        public void RunReportProgress(object sender, ProgressChangedEventArgs e)
        {
            object[] result = e.UserState as object[];
            List<Cell> cells = (List <Cell>)result[0];
            List<int> ids = (List<int>)result[1];

            for(int i = 0; i < ids.Count; i++)
                ThisField.CellField[ids[i]].CellState = cells[i].CellState;

            SaveField(true);
            CurStep++;
            AllSteps++;
            eventWaitHandle.Set();
        }

        public void RunReportProgress1(object sender, ProgressChangedEventArgs e)
        {
            List<Cell> cells = e.UserState as List<Cell>;

            for(int i = 0; i < ThisField.Rows * ThisField.Columns; i++)
            {
                ThisField.CellField[i].PosX = cells[i].PosX;
                ThisField.CellField[i].PosY = cells[i].PosY;
            }
            XSize = ThisField.Rows * CellSize;
            YSize = ThisField.Columns * CellSize;
            eventWaitHandleScale.Set();
        }

        public void RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(!e.Cancelled)
            {
                object[] result = e.Result as object[];
                List<Cell> cells = (List<Cell>)result[0];
                List<int> ids = (List<int>)result[1];

                for (int i = 0; i < ids.Count; i++)
                {
                    ThisField.CellField.ElementAt(ids.ElementAt(i)).CellState = cells.ElementAt(i).CellState;
                }

                SaveField(true);
                CurStep++;
                AllSteps++;
            }
            else
            {
                BackgroundWorker worker = sender as BackgroundWorker;
                worker.ProgressChanged -= RunReportProgress;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }

    public class RelayCommand : ICommand
    {
        private Action<object> execute;
        private Func<object, bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }
    }
}
