﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Cellular_automaton
{
    public enum State { Dead, FirstState, SecondState };

    public class Cell : INotifyPropertyChanged
    {
        private int posX;
        private int posY;
        private string colour;
        private State state;

        public int PosX
        {
            get { return posX; }
            set
            {
                posX = value;
                OnPropertyChanged("PosX");
            }
        }
        public int PosY
        {
            get { return posY; }
            set
            {
                posY = value;
                OnPropertyChanged("PosY");
            }
        }

        public string Colour
        {
            get { return colour; }
            set
            {
                colour = value;
                OnPropertyChanged("Colour");
            }
        }

        public State CellState
        {
            get { return state; }
            set
            {
                state = value;
                switch (state)
                {
                    case State.Dead:
                        Colour = "#FFFFFF";
                        break;
                    case State.FirstState:
                        Colour = "#000000";
                        break;
                    case State.SecondState:
                        Colour = "#00961C";
                        break;
                }
                OnPropertyChanged("CellState");
            }
        }

        public Cell()
        {
            posX = 0;
            posY = 0;
            colour = "";
            state = State.Dead;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
