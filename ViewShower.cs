﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Cellular_automaton
{
    class ViewShower
    {
        public static void Show(int p_viewIndex, object p_dataContext, bool p_isModal, Action<bool?> p_closeAction)
        {
            UserControl control = null;
            switch (p_viewIndex)
            {
                case 0:
                    control = new FieldSettings();
                    break;
                default:
                    throw new ArgumentOutOfRangeException("p_viewIndex", "Такого индекса View не существует");
            }
            if (control != null)
            {
                Window wnd = new Window();

                wnd.Width = 810;
                wnd.Height = 720;
                control.DataContext = p_dataContext;
                StackPanel sp = new StackPanel();
                sp.Children.Add(control);
                wnd.Content = sp;
                wnd.Closed += (s, e) => p_closeAction(wnd.DialogResult);
                wnd.Show();
            }
        }
    }
}
