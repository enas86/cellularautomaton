﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cellular_automaton
{
    class Field : INotifyPropertyChanged
    {
        private int rows;
        public int Rows
        {
            get { return rows; }
            set { rows = value; }
        }

        private int columns;
        public int Columns
        {
            get { return columns; }
            set { columns = value; }
        }

        private int speed;
        public int Speed
        {
            get { return speed; }
            set
            {
                speed = value;
                OnPropertyChanged("Speed");
            }
        }

        private ObservableCollection<Cell> cellField;
        public ObservableCollection<Cell> CellField
        {
            get { return cellField; }
            set { cellField = value; }
        }

        public Field(int r, int c)
        {
            cellField = new ObservableCollection<Cell>();
            rows = r;
            columns = c;

            Cell temp;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    temp = new Cell();
                    temp.PosX = i;
                    temp.PosY = j;
                    temp.CellState = State.Dead;
                    cellField.Add(temp);
                }
            }
        }

        public void Simulation(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            object[] pr = e.Argument as object[];

            Mutex mutex = pr[0] as Mutex;
            EventWaitHandle eventWaitHandle = pr[1] as EventWaitHandle;

            while (!e.Cancel)
            {
                try
                {
                    mutex.WaitOne(TimeSpan.FromSeconds(2));

                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        break;
                    }

                    UpdateField(sender, e);
                    worker.ReportProgress(0, e.Result);    
                    eventWaitHandle.WaitOne();                    
                    System.Threading.Thread.Sleep(350 - speed);
                }
                catch { continue; }
                finally
                {
                    mutex.ReleaseMutex();
                    worker.DoWork -= Simulation;
                }
            }
        }

        public void UpdateField(object sender, DoWorkEventArgs e)
        {
            List<Cell> result = new List<Cell>();
            List<int> id = new List<int>();

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Cell NewCell = new Cell();
                    int neighborsFS = 0, neighboursSecondSt = 0;
                    int L = 1, R = 1;

                    if (j != 0)
                    {
                        L = 0;
                        if (cellField.ElementAt(i * Columns + j - 1).CellState == State.FirstState) neighborsFS++;
                        else if (cellField.ElementAt(i * Columns + j - 1).CellState == State.SecondState) neighboursSecondSt++;
                    }

                    if (j != Columns - 1)
                    {
                        R = 0;
                        if (cellField.ElementAt(i * Columns + j + 1).CellState == State.FirstState) neighborsFS++;
                        else if (cellField.ElementAt(i * Columns + j + 1).CellState == State.SecondState) neighboursSecondSt++;
                    }

                    if (i != 0)
                    {
                        if (L == 0)
                        {
                            if (cellField.ElementAt((i - 1) * Columns + j - 1).CellState == State.FirstState) neighborsFS++;
                            else if (cellField.ElementAt((i - 1) * Columns + j - 1).CellState == State.SecondState) neighboursSecondSt++;
                        }
                        if (R == 0)
                        {
                            if (cellField.ElementAt((i - 1) * Columns + j + 1).CellState == State.FirstState) neighborsFS++;
                            else if (cellField.ElementAt((i - 1) * Columns + j + 1).CellState == State.SecondState) neighboursSecondSt++;
                        }

                        if (cellField.ElementAt((i - 1) * Columns + j).CellState == State.FirstState) neighborsFS++;
                        else if (cellField.ElementAt((i - 1) * Columns + j).CellState == State.SecondState) neighboursSecondSt++;
                    }

                    if (i != Rows - 1)
                    {
                        if (L == 0)
                        {
                            if (cellField.ElementAt((i + 1) * Columns + j - 1).CellState == State.FirstState) neighborsFS++;
                            else if (cellField.ElementAt((i + 1) * Columns + j - 1).CellState == State.SecondState) neighboursSecondSt++;
                        }
                        if (R == 0)
                        {
                            if (cellField.ElementAt((i + 1) * Columns + j + 1).CellState == State.FirstState) neighborsFS++;
                            else if (cellField.ElementAt((i + 1) * Columns + j + 1).CellState == State.SecondState) neighboursSecondSt++;
                        }

                        if (cellField.ElementAt((i + 1) * Columns + j).CellState == State.FirstState) neighborsFS++;
                        else if (cellField.ElementAt((i + 1) * Columns + j).CellState == State.SecondState) neighboursSecondSt++;
                    }

                    NewCell.PosX = cellField.ElementAt(i * Columns + j).PosX;
                    NewCell.PosY = cellField.ElementAt(i * Columns + j).PosY;

                    if (cellField.ElementAt(i * Columns + j).CellState == State.Dead)
                    {
                        if (neighborsFS + neighboursSecondSt == 3)
                        {
                            NewCell.CellState = (neighborsFS > neighboursSecondSt) ? State.FirstState : State.SecondState;//State.FirstState;
                            result.Add(NewCell);
                            id.Add(i * Columns + j);
                        }
                        else NewCell.CellState = State.Dead;
                    }
                    else
                    {
                        if (neighborsFS + neighboursSecondSt < 2 || neighborsFS + neighboursSecondSt > 3)
                        {
                            NewCell.CellState = State.Dead;
                            result.Add(NewCell);
                            id.Add(i * Columns + j);
                        }
                        else
                            NewCell.CellState = cellField.ElementAt(i * Columns + j).CellState;
                    }
                }
            }
            object[] res = new object[] { result, id };
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = res;
            worker.DoWork -= UpdateField;
        }

        public void PosTransform(object sender, DoWorkEventArgs e)
        {
            List<Cell> result = new List<Cell>();
            object[] parameters = e.Argument as object[];
            object[] progress = new object[2];

            BackgroundWorker scale = sender as BackgroundWorker;
            Mutex mutex = parameters[2] as Mutex;
            EventWaitHandle eventWaitHandleScale = parameters[3] as EventWaitHandle;

            try
            {
                mutex.WaitOne();

                foreach (Cell temp in CellField)
                {
                    Cell NewCell = new Cell();
                    NewCell.PosX = temp.PosX * (int)parameters[0] / (int)parameters[1];
                    NewCell.PosY = temp.PosY * (int)parameters[0] / (int)parameters[1];
                    result.Add(NewCell);
                }
                scale.ReportProgress(0, result);
                eventWaitHandleScale.WaitOne();
            }
            finally
            {
                mutex.ReleaseMutex();
            }

            scale.DoWork -= PosTransform;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
