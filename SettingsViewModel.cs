﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using System.Windows.Data;
using System.Globalization;
using System.Windows;
using System.Windows.Markup;
using System.Threading;

namespace Cellular_automaton
{
    class SettingsViewModel : INotifyPropertyChanged
    {
        private static EventWaitHandle eventWaitHandle = new AutoResetEvent(false);
        private static EventWaitHandle eventWaitHandleScale = new AutoResetEvent(false);
        static Mutex mutex = new Mutex(false, "SyncTool1");

        private BackgroundWorker worker;
        private BackgroundWorker Scale;

        public List<string> CellSt { get; set; }
        public List<string> BrushSt { get; set; }

        private Field field;
        public Field ThisField
        {
            get { return field; }
        }

        private int xSize;
        public int XSize
        {
            get { return xSize; }
            set
            {
                xSize = value;
                OnPropertyChanged("XSize");
            }
        }

        private int ySize;
        public int YSize
        {
            get { return ySize; }
            set
            {
                ySize = value;
                OnPropertyChanged("YSize");
            }
        }

        private int xFieldSize;
        public int XFieldSize
        {
            get { return xFieldSize; }
            set
            {
                xFieldSize = value;
                OnPropertyChanged("XFieldSize");
            }
        }

        private int yFieldSize;
        public int YFieldSize
        {
            get { return yFieldSize; }
            set
            {
                yFieldSize = value;
                OnPropertyChanged("YFieldSize");
            }
        }

        private string brushMode;
        public string BrushMode
        {
            get { return brushMode; }
            set
            {
                if (value == "Drawing(hover on cell)") brushMode = "Hover";
                else brushMode = "Press";
                OnPropertyChanged("BrushMode");
            }
        }

        private State brushState;

        private string cellColour;
        public string CellColour
        {
            get { return cellColour; }
            set 
            {
                cellColour = value;
                switch(value)
                {
                    case "Dead(white)":
                        brushState = State.Dead;
                        break;
                    case "Alive State 1(black)":
                        brushState = State.FirstState;
                        break;
                    case "Alive State 2(green)":
                        brushState = State.SecondState;
                        break;
                }
            }
        }
        
        public SettingsViewModel(int rows, int columns)
        {
            CellSt = new List<string> { "Dead(white)", "Alive State 1(black)", "Alive State 2(green)" };
            BrushSt = new List<string> { "Press", "Drawing(hover on cell)" };

            field = new Field(rows, columns);
            CellSize = 10;
            foreach (Cell temp in ThisField.CellField)
            {
                temp.PosX *= cellSize;
                temp.PosY *= cellSize;
            }

            BrushMode = "Press";
            CellColour = "Dead(white)";
            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.RunWorkerCompleted += RunWorkerCompleted;

            Scale = new BackgroundWorker();
            Scale.WorkerReportsProgress = true;
            Scale.ProgressChanged += RunReportProgress1;
        } 

        private int cellSize;
        public int CellSize
        {
            get { return cellSize; }
            set
            {
                cellSize = value;
                OnPropertyChanged("CellSize");
            }
        }

        public void RunReportProgress1(object sender, ProgressChangedEventArgs e)
        {
            List<Cell> cells = e.UserState as List<Cell>;
                
            for (int i = 0; i < ThisField.Rows * ThisField.Columns; i++)
            {
                ThisField.CellField.ElementAt(i).PosX = cells.ElementAt(i).PosX;
                ThisField.CellField.ElementAt(i).PosY = cells.ElementAt(i).PosY;
            }

            XFieldSize = ThisField.Rows * CellSize;
            YFieldSize = ThisField.Columns * CellSize;
            eventWaitHandleScale.Set();
        }

        private RelayCommand setSize;
        public RelayCommand SetSize
        {
            get
            {
                return setSize ??
                  (setSize = new RelayCommand(obj =>
                  {
                      worker.DoWork += Update;
                      worker.ProgressChanged += DelField;             
                      worker.RunWorkerAsync();
                  }, (obj) => (XSize >= 0 && XSize <= 100) && (YSize >= 0 && YSize <= 100)));
            }
        }

        private RelayCommand changeSize;
        public RelayCommand ChangeSize
        {
            get
            {
                return changeSize ??
                  (changeSize = new RelayCommand(obj =>
                  {
                      if (CellSize >= 5 && CellSize <= 20)
                      {
                          int old = cellSize;
                          CellSize = old + Convert.ToInt32(obj.ToString());
                          if (CellSize > 20) CellSize--;
                          else 
                          { 
                              if (CellSize < 5) CellSize++;
                              else
                              {
                                  object[] pr = new object[] { cellSize, old, mutex, eventWaitHandleScale };

                                  Scale.DoWork += field.PosTransform;
                                  Scale.RunWorkerAsync(pr);
                              }
                          }
                      }

                  }, (obj) => !Scale.IsBusy));
            }
        }

        private RelayCommand changeColour;
        public RelayCommand ChangeColour
        {
            get
            {
                return changeColour ??
                  (changeColour = new RelayCommand(obj =>
                  {
                      Cell Temp = obj as Cell;
                      Temp.CellState = brushState;
                  }));
            }
        }

        public void Update(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker Curworker = sender as BackgroundWorker;

            Field NewField = new Field(ThisField.Rows, ThisField.Columns);
            for (int i = 0; i < ThisField.Rows * ThisField.Columns; i++)
            {
                NewField.CellField.ElementAt(i).CellState = ThisField.CellField.ElementAt(i).CellState;
                NewField.CellField.ElementAt(i).PosX = ThisField.CellField.ElementAt(i).PosX;
                NewField.CellField.ElementAt(i).PosY = ThisField.CellField.ElementAt(i).PosY;
            }
            NewField.Columns = ThisField.Columns;
            NewField.Rows = ThisField.Rows;

            Curworker.ReportProgress(0);
            System.Threading.Thread.Sleep(150);
            Curworker.ProgressChanged += RunReportProgress;
            Curworker.ProgressChanged -= DelField;

            for (int i = 0; i < xSize; i++)
            {
                List<Cell> NewCells = new List<Cell>();
                for (int j = 0; j < ySize; j++)
                {
                    if (i < NewField.Rows && j < NewField.Columns)
                        NewCells.Add(NewField.CellField.ElementAt(i * NewField.Columns + j));// = ThisField.CellField.ElementAt(i * ThisField.Columns + j).Colour;
                    else
                    {
                        Cell temp = new Cell();
                        temp.CellState = State.Dead;
                        temp.PosX = i * cellSize;
                        temp.PosY = j * cellSize;
                        NewCells.Add(temp);
                    }
                }
                Curworker.ReportProgress(0, NewCells);
            }
        }

        public void RunReportProgress(object sender, ProgressChangedEventArgs e)
        {  
            List<Cell> NewCells = e.UserState as List<Cell>;

            foreach (Cell temp in NewCells) ThisField.CellField.Add(temp);
        }

        public void DelField(object sender, ProgressChangedEventArgs e)
        {
            ThisField.CellField.Clear();
            ThisField.Rows = XSize;
            ThisField.Columns = YSize;
        }

        public void RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker Curworker = sender as BackgroundWorker;
            Curworker.ProgressChanged -= RunReportProgress;
            Curworker.DoWork -= Update;

            XFieldSize = ThisField.Rows * CellSize;
            YFieldSize = ThisField.Columns * CellSize;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
